package com.example.funforkids.data.local.model.schema

import org.jetbrains.exposed.dao.id.IntIdTable

object WordsGER : IntIdTable() {
    val word = varchar("word", 255)
    val imageId = reference("imageId", Images)
    val audio = blob("audio")
}