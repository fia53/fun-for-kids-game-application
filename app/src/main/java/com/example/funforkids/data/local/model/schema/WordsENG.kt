package com.example.funforkids.data.local.model.schema
import org.jetbrains.exposed.dao.id.IntIdTable

// Tabelle für englische Wörter mit Verweis auf das Bild und eigenem Audio
object WordsENG : IntIdTable() {
    var word = varchar("word", 255)
    val imageId = reference("imageId", Images)
    val audio = blob("audio")
}