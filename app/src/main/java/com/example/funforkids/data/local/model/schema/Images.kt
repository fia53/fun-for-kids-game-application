package com.example.funforkids.data.local.model.schema

import org.jetbrains.exposed.dao.id.IntIdTable

object Images : IntIdTable() {
    val image = blob("image")
}