package com.example.funforkids.data.local

interface WordsAPI {
    fun <T> query(statement: () -> T): T
}