package com.example.funforkids.data.local.model.dao

import com.example.funforkids.data.local.model.schema.WordsGER
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.transactions.transaction


class WordG(id: EntityID<Int>) : IntEntity(id) {
    //companion object =>Verbindung zwischen der WordG und der WordsGER Tabelle
    companion object : IntEntityClass<WordG>(WordsGER)

    var word by WordsGER.word
    var image by Image referencedOn WordsGER.imageId
    var audio by WordsGER.audio

    // Zugriff auf das Bild als ByteArray
   val imageData: ByteArray
        get() = transaction { // Zugriff auf die Datenbank
            // Image-Klasse
            image.image.bytes
        }
    val audioData: ByteArray
        get() = transaction { // Zugriff auf die Datenbank
            // Image-Klasse
            audio.bytes
        }
}