package com.example.funforkids.data.local.model.dao

import com.example.funforkids.data.local.model.schema.WordsENG
import com.example.funforkids.data.local.model.schema.WordsGER
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.transactions.transaction

class WordE (id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<WordE>(WordsENG) // Tabelle

    var word by WordsENG.word
    var image by Image referencedOn WordsENG.imageId
    var audioENG by WordsENG.audio

    // Zugriff auf das Bild als ByteArray
    val imageData: ByteArray
        get() = transaction { // Zugriff auf die Datenbank
            image.image.bytes
        }
}
