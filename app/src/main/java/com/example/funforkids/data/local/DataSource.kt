package com.example.funforkids.data.local

import android.content.res.AssetManager
import com.example.funforkids.data.local.model.dao.Image
import com.example.funforkids.data.local.model.dao.WordE
import com.example.funforkids.data.local.model.dao.WordG
import com.example.funforkids.data.local.model.schema.Images
import com.example.funforkids.data.local.model.schema.WordsENG
import com.example.funforkids.data.local.model.schema.WordsGER
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.InputStream

class DataSource(
    private val filesDir: File,
    private val assets: AssetManager
) : WordsAPI {
    private val db by lazy {
        Database.connect("jdbc:h2:$filesDir/db").apply {
            transaction(this) {
                SchemaUtils.drop(WordsGER, WordsENG, Images)
                SchemaUtils.create(WordsGER, WordsENG, Images)


               //val imageBytesBanane = loadImageBytes("banane.png")
               // val audioBytesBananeGER = loadAudioBytes("banane.mp3")
              //  val audioBytesBananaENG = loadAudioBytes("banana.mp3")
                val imageBytesBall = loadImageBytes("ball.png")
                val audioBytesBallGER = loadAudioBytes("ball_ger.mp3")
                val audioBytesBallENG = loadAudioBytes("ball_eng.mp3")
              //  val imageBytesApfel = loadImageBytes("apfel.png")
              //  val audioBytesApfelGER = loadAudioBytes("apfel.mp3")
             //   val audioBytesApfelENG = loadAudioBytes("apple.mp3")
                val imageBytesKatze = loadImageBytes("katze.png")
                val audioBytesKatze = loadAudioBytes("katze.mp3")
                val audioBytesCat = loadAudioBytes("cat.mp3")
                val imageBytesHund = loadImageBytes("hund.png")
                val audioBytesHund = loadAudioBytes("hund.mp3")
                val audioBytesDog = loadAudioBytes("dog.mp3")
                val imageBytesHaus = loadImageBytes("haus.png")
                val audioBytesHaus = loadAudioBytes("haus.mp3")
                val audioBytesHouse = loadAudioBytes("house.mp3")
               // val imageBytesStuhl = loadImageBytes("stuhl.png")
             //   val audioBytesStuhl = loadAudioBytes("stuhl.mp3")
              //  val audioBytesChair = loadAudioBytes("chair.mp3")
              //  val imageBytesWasser = loadImageBytes("wasser.png")
              //  val audioBytesWasser = loadAudioBytes("wasser.mp3")
              //  val audioBytesWater = loadAudioBytes("water.mp3")
               // val imageByteFlasche = loadImageBytes("flasche.png")
               // val audioBytesFlasche = loadAudioBytes("flasche.mp3")
               // val audioBytesBottle = loadAudioBytes("bottle.mp3")


                /*
                ***BANANE***

                val banane = Image.new {
                    this.image = ExposedBlob(imageBytesBanane)
                }
                WordG.new {
                    word = "BANANE"
                    image = banane
                    audio = ExposedBlob(audioBytesBananeGER)
                }
                WordE.new {
                    word= "BANANA"
                    image = banane
                    audioENG = ExposedBlob(audioBytesBananaENG)
                }
                */
                /****BALL***
 */
                val ball = Image.new {
                    this.image = ExposedBlob(imageBytesBall)
                }
                WordG.new {
                    word = "BALL"
                    image = ball
                    audio = ExposedBlob(audioBytesBallGER)
                }
                WordE.new {
                    word= "BALL"
                    image = ball
                    audioENG = ExposedBlob(audioBytesBallENG)
                }
                /*
                /* ***APFEL***   */

                val apfel = Image.new {
                    this.image = ExposedBlob(imageBytesApfel)
                }
                WordG.new {
                    word = "APFEL"
                    image = apfel
                    audio = ExposedBlob(audioBytesApfelGER)
                }
                WordE.new {
                    word= "APPLE"
                    image = apfel
                    audioENG = ExposedBlob(audioBytesApfelENG)
                }

                 */

                /* ****Katze**** */

                val katze = Image.new {
                    this.image = ExposedBlob(imageBytesKatze)
                }
                WordG.new {
                    word = "KATZE"
                    image = katze
                    audio = ExposedBlob(audioBytesKatze)
                }
                WordE.new {
                    word= "CAT"
                    image = katze
                    audioENG = ExposedBlob(audioBytesCat)
                }

                /* ++++Hund++++ */

                val hund = Image.new {
                    this.image = ExposedBlob(imageBytesHund)
                }
                WordG.new {
                    word = "HUND"
                    image = hund
                    audio = ExposedBlob(audioBytesHund)
                }
                WordE.new {
                    word= "DOG"
                    image = hund
                    audioENG = ExposedBlob(audioBytesDog)
                }


                /* ++++Haus++++ */

                val haus = Image.new {
                    this.image = ExposedBlob(imageBytesHaus)
                }
                WordG.new {
                    word = "HAUS"
                    image = haus
                    audio = ExposedBlob(audioBytesHaus)
                }
                WordE.new {
                    word= "HOUSE"
                   image = haus
                   audioENG = ExposedBlob(audioBytesHouse)
               }
/*
                /* ++++Stuhl+++ */

                val stuhl = Image.new {
                    this.image = ExposedBlob(imageBytesStuhl)
                }
                WordG.new {
                    word = "STUHL"
                    image = stuhl
                    audio = ExposedBlob(audioBytesStuhl)
                }
                WordE.new {
                    word= "CHAIR"
                    image = stuhl
                    audioENG = ExposedBlob(audioBytesChair)
                }

 */
/*
                /* ++++Wasser+++ */

                val wasser = Image.new {
                    this.image = ExposedBlob(imageBytesWasser)
                }
                WordG.new {
                    word = "WASSER"
                    image = wasser
                    audio = ExposedBlob(audioBytesWasser)
                }
                WordE.new {
                    word= "WATER"
                    image = wasser
                    audioENG = ExposedBlob(audioBytesWater)
                }

                /* ++++Flasche+++ */

                val flasche = Image.new {
                    this.image = ExposedBlob(imageByteFlasche)
                }
                WordG.new {
                    word = "FLASCHE"
                    image = flasche
                    audio = ExposedBlob(audioBytesFlasche)
                }
                WordE.new {
                    word= "BOTTLE"
                    image = flasche
                    audioENG = ExposedBlob(audioBytesBottle)
                }

*/
            }
        }
    }

//    // Methode zum Erstellen des Schemas und Einfügen eines neuen Worts
//    fun insertWord() {
//        transaction(db) {
//            // Bild und Audio laden (Beispiel: Ersatz mit Ihrem tatsächlichen Bild und Audio)
//            val imageBytes = loadImageBytes("haus.jpg")
//            val audioBytes = loadAudioBytes("haus.mp3")
//
//            // Neues Wort in der Datenbank erstellen
//            val haus = WordG.new {
//                germanWord = "HAUS"
//                image = Image.new {
//                    this.image = ExposedBlob(imageBytes)
//                }
//                audio = ExposedBlob(audioBytes)
//            }
//
//            println("Neues deutsches Wort erstellt: ${haus.germanWord}")
//        }
//    }

    // Methode zum Laden von Bildern aus dem Assets-Ordner

    private fun loadImageBytes(fileName: String): ByteArray {
        val inputStream: InputStream = assets.open(fileName)
        val outputStream = ByteArrayOutputStream()
        inputStream.use { input ->
            outputStream.use { output ->
                input.copyTo(output)
            }
        }
        return outputStream.toByteArray()
    }

    // Methode zum Laden von Audiodateien aus dem Assets-Ordner
    private fun loadAudioBytes(fileName: String): ByteArray {
        // Implementieren Sie entsprechend
        return loadImageBytes(fileName)
    }

    override fun <T> query(statement: () -> T): T =
        transaction(db) { statement() } // Zugriff auf die Datenbank

}