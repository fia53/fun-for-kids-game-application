package com.example.funforkids.data

import android.content.Context
import android.content.res.AssetManager
import com.example.funforkids.data.Repository.Configuration.filesDir
import com.example.funforkids.data.Repository.Configuration.assets
import com.example.funforkids.data.local.DataSource
import com.example.funforkids.data.local.WordsAPI
import java.io.File

object Repository : WordsAPI by DataSource(filesDir, assets) {
    object Configuration {
        lateinit var filesDir: File // Zugriff auf das Verzeichnis, in dem die Dateien gespeichert werden
        lateinit var assets: AssetManager
    }
}