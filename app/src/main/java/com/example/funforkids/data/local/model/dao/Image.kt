package com.example.funforkids.data.local.model.dao

import com.example.funforkids.data.local.model.schema.Images
import com.example.funforkids.data.local.model.schema.WordsGER
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.transactions.transaction
// Zugriff auf das Bild als ByteArray
class Image (id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Image>(Images)

    var image by Images.image // Zugriff auf das Bild als ByteArray

}



