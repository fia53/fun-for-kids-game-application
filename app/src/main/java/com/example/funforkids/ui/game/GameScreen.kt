package com.example.funforkids.ui.game

import android.content.ClipData
import android.content.ClipDescription
import android.content.Context
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.funforkids.R
import androidx.compose.ui.draganddrop.DragAndDropTransferData
import androidx.compose.foundation.draganddrop.dragAndDropSource
import androidx.compose.foundation.draganddrop.dragAndDropTarget
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.IconButton
import androidx.compose.ui.draganddrop.DragAndDropEvent
import androidx.compose.ui.draganddrop.DragAndDropTarget
import androidx.compose.ui.draganddrop.mimeTypes
import androidx.compose.ui.draganddrop.toAndroidDragEvent
import androidx.compose.ui.res.painterResource
import androidx.core.net.toUri
import androidx.lifecycle.LifecycleOwner
import com.example.funforkids.MainActivity
import java.io.File
import java.io.FileOutputStream


@OptIn(ExperimentalFoundationApi::class) //bedeutet dass die Funktionen, die mit Drag and Drop zusammenhängen, experimentell sind

@Composable
fun GameScreen(
    viewModel: GameViewModel,
    navigateHome: (String) -> Unit,
    navigate: (String) -> Unit,
    lastWord: String?
) {

    var _lastWord = lastWord
    viewModel.generateLastWord(_lastWord)

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Text(
            text = if (MainActivity.GlobalState.selectedLanguage == "german")
                "Ziehe die Buchstaben in die Felder in der richtigen Reihenfolge." else "Drag the letters into the fields in the correct order.",
            modifier = Modifier.align(Alignment.CenterHorizontally),
            style = TextStyle(
                fontSize = 30.sp,
                fontFamily = FontFamily(Font(R.font.baloo_tammudu)),
                fontWeight = FontWeight.Bold,
                color = Color(0xFF103D78),
                textAlign = TextAlign.Center,
                lineHeight = 40.sp * 0.8 //  Zeilenabstand anzupassen
            )
        )

//********Images as ByteArray********
        Image(
            bitmap = BitmapFactory.decodeByteArray( // Bild aus ByteArray laden und anzeigen lassen (BitmapFactory) => asImageBitmap konvertieren und anzeigen lassen (Image)
                // => contentDescription hinzufügen => contentScale hinzufügen => modifier hinzufügen
                viewModel.state.currentImage, // ByteArray aus dem ViewModel holen und in ein Bild umwandeln und anzeigen lassen (Image)
                0, // Offset
                viewModel.state.currentImage.size // Länge des ByteArrays (Bildgröße) holen und anzeigen lassen (Image)
            ).asImageBitmap(), // ByteArray  as an Image to display
            contentDescription = "image description",
            contentScale = ContentScale.Crop, //fit the image within the bounds of the layout
            modifier = Modifier
                .width(300.dp)
                .height(300.dp),
        )

//****Swierigkeitsstufe 2 und 3****
        if (viewModel.state.difficultyLevel != "LEVEL 2" && viewModel.state.difficultyLevel != "LEVEL 3") {
            Text(
                text = viewModel.state.currentWord, // Aktuelles Wort anzeigen lassen (Text) => style hinzufügen => modifier hinzufügen
                style = TextStyle(
                    fontSize = 70.sp,
                    fontFamily = FontFamily(Font(R.font.baloo_tammudu)),
                    fontWeight = FontWeight(400),
                    color = Color(0xFF103D78),
                    lineHeight = 70.sp // Ändere den Wert, um den Zeilenabstand anzupassen
                ),
                modifier = Modifier.height(110.dp) // Höhe des Textes

            )
        }
//FELDER GENERIEREN
        // Anzeige der grauen Felder für das aktuelle Wort
       // val emptyFieldsCount = viewModel.state.currentWord.length // nach Wortlänge
        LazyRow(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(vertical = 10.dp)
        ) {
            // Leere Felder für jedes Zeichen des Wortes

            itemsIndexed(viewModel.state.playerWord) { index,
                                                       playerLetter ->// Feld Wiederholung für jedes Zeichen des Wortes

                Box(
                    modifier = Modifier
                        .width(45.dp)
                        .height(80.dp)
                        .background(
                            color = Color.LightGray,
                            shape = RectangleShape
                        )
                        .dragAndDropTarget( // (Feld)
                            shouldStartDragAndDrop = { event -> // Überprüfen, ob das Feld ein Drag-and-Drop-Ereignis starten soll oder nicht (Feld)
                                event // Event überprüfen (Feld) => clipDescription überprüfen (Feld) => mimeTypes überprüfen (Feld) => contains überprüfen (Feld)
                                    .mimeTypes() //die methode wird verwendet, um die Art der Daten zu identifizieren, die beim Drag-and-Drop in einer Jetpack Compose-Anwendung übertragen werden
                                    .contains(ClipDescription.MIMETYPE_TEXT_PLAIN) // Überprüfen, ob die mimeTypes die ClipDescription.MIMETYPE_TEXT_INTENT enthalten (Feld)
                                viewModel.state.playerWord[index] == ' ' // wenn das Feld leer ist
                            },

                            target = object :
                                DragAndDropTarget { //  (Feld)
                                override fun onDrop(event: DragAndDropEvent): Boolean { // onDrop  passiert nachdem man  die Buchstabe abgelegt /losgelassen wird
                                    viewModel.setLetter(
                                        index,
                                        event.toAndroidDragEvent().clipData.description.label[0]
                                    ) // Buchstabe in das Feld einfügen (Feld)
                                    return true  // erfolgreiches ablegen
                                }
                            }
                        )) {

                    Text(
                        text = playerLetter.toString(), // Buchstabe anzeigen lassen (Text)
                        style = TextStyle(
                            fontSize = 48.sp,
                            fontFamily = FontFamily(Font(R.font.baloo_tammudu)),
                            fontWeight = FontWeight.Normal,
                            color = Color(0xFF103D78)
                        ),
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .align(Alignment.Center)
                    )
                }

                Spacer(modifier = Modifier.width(16.dp))
            }
        }

//BUCHSTABEN GENERIEREN
        LazyRow {
            itemsIndexed(
                items = viewModel.state.letters,
                key = { index, letter -> "$index$letter" }
            ) { _, letter -> // Buchstaben generieren

                Box(
                    modifier = Modifier
                        .padding(8.dp)
                        .dragAndDropSource {  //(Buchstaben)
                            detectTapGestures(
                                onPress = {
                                    startTransfer(
                                        DragAndDropTransferData( // Drag and Drop Transfer Data hinzufügen (Buchstabe)
                                            clipData = ClipData.newPlainText(
                                                "$letter", "$letter" // Buchstabe hinzufügen
                                            )
                                        )
                                    )
                                }
                            )
                        }
                ) {
                    Text(
                        text = letter.toString(),
                        style = TextStyle(
                            fontSize = 48.sp,
                            fontFamily = FontFamily(Font(R.font.baloo_tammudu)),
                            fontWeight = FontWeight.Normal,
                            color = Color(0xFF103D78)
                        ),
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .align(Alignment.Center)

                    )
                }
            }
        }
        // Zustand für die Anzeige des nächsten Wortes
        var shouldShowNextWord by remember { mutableStateOf(false) }

        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            // Logik zum Anzeigen des nächsten Wortes
            if (shouldShowNextWord) {
                viewModel.generateNextWord() // Generiere das nächste Wort
                shouldShowNextWord = false // Setze den Zustand zurück
            }
            val context = LocalContext.current
            Row {

                // ****SOUND BUTTON****
                IconButton(
                    onClick = { playClickSound(context, viewModel.state.currentAudio) },
                    modifier = Modifier.padding(bottom = 10.dp)
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.speaker),
                        contentDescription = "Speaker Icon"
                    )
                }
                // ****HOME Button****
                IconButton(
                    onClick = { navigateHome("main") },
                    modifier = Modifier.padding(bottom = 10.dp, start = 25.dp)
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.home_icon),
                        contentDescription = "Home Icon"
                    )
                }
                //****FORWARD BUTTON****
                IconButton(
                    onClick = { checkWord(viewModel, navigate, context) },
                    modifier = Modifier.padding(bottom = 10.dp, start = 25.dp)
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.arrow),
                        contentDescription = "Forward Icon"
                    )
                }
            }
        }
    }
}

fun checkWord(
    viewModel: GameViewModel,
    navigate: (String) -> Unit,
    context: Context
) {
    //**** check & show next word (  if all field filld  &  letters in corect order) ****
    if (viewModel.checkAndShowNextWord()) {

        //*** add current word to the list of success words ***
        MainActivity.GlobalState.successWords =
            MainActivity.GlobalState.successWords.plus(viewModel.state.currentWord)
        navigate("final_success/${viewModel.state.difficultyLevel}")

        //playlist according selected language
        if (MainActivity.GlobalState.selectedLanguage == "german") {

            val audioList = listOf(R.raw.gut_gemacht, R.raw.das_ist_korrekt, R.raw.ausgezeichnet)
            val mediaPlayer =
                MediaPlayer.create(context, audioList.random()) // Soundeffekt)
            mediaPlayer.start()
            mediaPlayer.setOnCompletionListener {
                mediaPlayer.release()
            }
        } else {
            val audioList = listOf(R.raw.well_done, R.raw.this_is_correct, R.raw.great)
            val mediaPlayer = MediaPlayer.create(context,audioList.random()) // Soundeffekt)
            mediaPlayer.start()

            mediaPlayer.setOnCompletionListener {
                mediaPlayer.release()
            }
        }

    } else {
        navigate("final_unsuccess/${viewModel.state.difficultyLevel}?lastWord=${viewModel.state.currentWord}")
        if (MainActivity.GlobalState.selectedLanguage == "german") {
            val mediaPlayer =
                MediaPlayer.create(context, R.raw.versuche_noch_mal) // Soundeffekt)
            mediaPlayer.start()

            mediaPlayer.setOnCompletionListener {
                mediaPlayer.release()
            }
        } else {
            val mediaPlayer = MediaPlayer.create(context, R.raw.tryagain) // Soundeffekt)
            mediaPlayer.start()

            mediaPlayer.setOnCompletionListener {
                mediaPlayer.release()
            }
        }
    }
}
fun playClickSound(context: Context, audioBytes: ByteArray) {

    try {
        val tempFile = File.createTempFile("tempAudio", ".mp3", context.cacheDir).apply {
            // Delete the file when the app exits
            deleteOnExit()
        }
        // Write audio data to the file
        FileOutputStream(tempFile).use { output ->
            output.write(audioBytes)
        }
        val mediaPlayer = MediaPlayer.create(context, tempFile.toUri()) // toUri bedeutet dass es in Uri umgewandelt wird
        mediaPlayer.start()
        mediaPlayer.setOnCompletionListener {
            mediaPlayer.release()
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

}



