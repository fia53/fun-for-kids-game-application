package com.example.funforkids.ui.final

data class FinalState(
    val finalSuccessCheck: Boolean = true,
    val finalSuccessMessage: String = "Great Job!",
    val currentWord: String = "BANANE"
) {

}