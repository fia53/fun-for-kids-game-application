package com.example.funforkids.ui.final

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.funforkids.MainActivity
import com.example.funforkids.R
import com.example.funforkids.ui.home.HomeViewModel

@Composable
fun NextLevelScreen(
    viewModel: HomeViewModel,
    navigateHome: (String) -> Unit,
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = if(MainActivity.GlobalState.selectedLanguage == "german")
                "Du bist bereit für das nächste LEVEL!" else "You are ready for the next LEVEL!",

        modifier = Modifier.align(Alignment.CenterHorizontally),
        style = TextStyle(
            fontSize = 30.sp,
            fontFamily = FontFamily(Font(R.font.baloo_tammudu)),
            fontWeight = FontWeight.Bold,
            color = Color(0xFF103D78),
            textAlign = TextAlign.Center,
            lineHeight = 40.sp * 0.8 // Ändere den Wert, um den Zeilenabstand anzupassen
        )
    )
        @Suppress("ResourceType") //unterdrücken Sie Warnungen, die aufgrund der Verwendung von R.drawable.next_level auftreten
        (Image(
        painter = painterResource(id = R.drawable.next_level),
        contentDescription = "Great Job GIF",
        modifier = Modifier.size(350.dp),
        contentScale = ContentScale.Crop
    ))
        // HOME Button
        IconButton(
            onClick = { navigateHome("main") },
            modifier = Modifier.padding(bottom = 10.dp, start = 25.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.home_icon),
                contentDescription = "Home Icon"
            )
        }
    }
}