package com.example.funforkids.ui.home
import android.media.MediaPlayer
import androidx.compose.foundation.Image

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.net.toUri
import com.example.funforkids.MainActivity
import com.example.funforkids.R


@Composable
fun LevelScreen(viewModel: HomeViewModel, navigate: (String) -> Unit) {
   // val selectedLevel = viewModel.selectedLevel.value
    val difficultyLevels = viewModel.difficultyLevels
    val mContext = LocalContext.current // Aktuelle context für MediaPlayer

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.homepage1),
            contentDescription = "HomePagePicture",
            contentScale = ContentScale.FillBounds, // Bild an den Bildschirm anpassen
            modifier = Modifier
                // .fillMaxSize(0.6f)
                .padding(bottom = 16.dp)// Abstand  unteren Rand
                .clip(RoundedCornerShape(27.dp))// Ecken abrunden
        )
        Box() {
            Text(
                text = "fun ABC kids",
                style = TextStyle(
                    fontSize = 40.sp,
                    fontFamily = FontFamily(Font(R.font.baloo_tammudu)),
                    //fontWeight = FontWeight(1000),
                    fontWeight = FontWeight.Bold,
                    color = Color(0xFF103D78),
                    textAlign = TextAlign.Center
                ),
                modifier = Modifier.align(Alignment.Center)
            )
        }

        // Levels
        difficultyLevels.forEach { level ->
            Button(
                onClick = {
                    navigate(level);  // Zustand für die Audiowiedergabe
                    if (MainActivity.GlobalState.selectedLanguage == "german") {
                        val mediaPlayer =
                            MediaPlayer.create(mContext, R.raw.drag_de) // Soundeffekt)
                        mediaPlayer.start()

                        mediaPlayer.setOnCompletionListener {
                            mediaPlayer.release()
                        }
                    } else {
                        val mediaPlayer = MediaPlayer.create(mContext, R.raw.drag) // Soundeffekt)
                        mediaPlayer.start()
                        mediaPlayer.setOnCompletionListener {//play only once
                            mediaPlayer.release()
                        }
                    }
                },
                modifier = Modifier.padding(bottom = 8.dp)
                    .height(50.dp)
                    .width(200.dp)
            ) {
                Text(
                    text = level,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontFamily = FontFamily(Font(R.font.roboto_mono)),
                        fontWeight = FontWeight(700),
                        color = Color(0xFFFFFFFF),
                        textAlign = TextAlign.Center,
                    )
                )
            }
        }
        LanguageSelectionButtons(viewModel)
    }
}

@Composable
fun LanguageSelectionButtons(viewModel: HomeViewModel) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        ElevatedButton(
            onClick = { MainActivity.GlobalState.selectedLanguage = "german" },
            modifier = Modifier.padding(horizontal = 8.dp),
            colors = ButtonDefaults.buttonColors(containerColor = Color.DarkGray),

            ) {
            Text(text = "DE", color = Color.White)
        }

        ElevatedButton(
            onClick = { MainActivity.GlobalState.selectedLanguage = "english" },
            modifier = Modifier.padding(horizontal = 8.dp),
            colors = ButtonDefaults.buttonColors(containerColor = Color.DarkGray)
        ) {
            Text(text = "EN", color = Color.White)
        }
    }
}
