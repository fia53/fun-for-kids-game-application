package com.example.funforkids.ui.final

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.funforkids.R
import com.example.funforkids.ui.home.HomeViewModel

import androidx.compose.foundation.Image
import androidx.compose.material3.IconButton
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.style.TextAlign
import com.example.funforkids.MainActivity

@Composable
fun FinalScreen_Unsuccess(viewModel: FinalViewModel,
                          navigateBack: () -> Unit,
){
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = if(MainActivity.GlobalState.selectedLanguage == "german")
                "Versuche es noch einmal!" else "Try Again!",
            modifier = Modifier.align(Alignment.CenterHorizontally),
            style = TextStyle(
                fontSize = 40.sp,
                fontFamily = FontFamily(Font(R.font.baloo_tammudu)),
                fontWeight = FontWeight.Bold,
                color = Color(0xFF103D78),
                textAlign = TextAlign.Center,
                lineHeight = 40.sp * 0.8 // change the value to adjust line height
            )
        )
        @Suppress("ResourceType")
        Image(
            painter = painterResource(id = R.drawable.test2),
            contentDescription = "Try Again GIF",
            modifier = Modifier.size(350.dp),
            contentScale = ContentScale.Crop
        )

        // Back to the game level same currentword
        IconButton(
            onClick = { navigateBack()  },
            modifier = Modifier.padding(top = 16.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.left),
                contentDescription = "Forward Icon"
            )
        }
    }
}