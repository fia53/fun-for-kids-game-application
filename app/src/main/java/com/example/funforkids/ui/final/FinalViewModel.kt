package com.example.funforkids.ui.final

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class FinalViewModel : ViewModel() {
    var state by mutableStateOf(FinalState())

    fun setCurrentWord(currentWord: String) {
     state=state.copy(currentWord= currentWord)
    }
}