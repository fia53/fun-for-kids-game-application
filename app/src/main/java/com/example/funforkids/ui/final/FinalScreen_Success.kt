import android.media.MediaPlayer
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.funforkids.MainActivity
import com.example.funforkids.R
import com.example.funforkids.ui.home.HomeViewModel

@Composable
fun FinalScreen_Success(
    viewModel: HomeViewModel,
    navigate: () -> Unit,
    navigateNextLevel: () -> Unit,
    difficultyLevel: String,
    navigateFinal: () -> Unit
) {
    val context = LocalContext.current
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = if (MainActivity.GlobalState.selectedLanguage == "german")
                "Gut gemacht!" else "Well done",
            modifier = Modifier.align(Alignment.CenterHorizontally),
            style = TextStyle(
                fontSize = 40.sp,
                fontFamily = FontFamily(Font(R.font.baloo_tammudu)),
                fontWeight = FontWeight.Bold,
                color = Color(0xFF103D78),
                textAlign = TextAlign.Center,
                lineHeight = 40.sp * 0.8 // Ändere den Wert, um den Zeilenabstand anzupassen
            )
        )
        @Suppress("ResourceType")
        Image(
            painter = painterResource(id = R.drawable.great),
            contentDescription = "Great Job",
            modifier = Modifier.size(350.dp),
            contentScale = ContentScale.Crop
        )

        // Back to the same level next random word
        IconButton(
            onClick = {
                if (MainActivity.GlobalState.successWords.size < MainActivity.GlobalState.wordCount) {
                    navigate()
                } else if (difficultyLevel != "LEVEL 3") {
                    navigateNextLevel()
                    if (MainActivity.GlobalState.selectedLanguage == "german") {


                        val mediaPlayer =
                            MediaPlayer.create(context, R.raw.bereit_next_level) // Soundeffekt)
                        mediaPlayer.start()

                        mediaPlayer.setOnCompletionListener {
                            mediaPlayer.release()
                        }
                    } else {
                        val mediaPlayer =
                            MediaPlayer.create(context, R.raw.ready_next_level) // Soundeffekt)
                        mediaPlayer.start()

                        mediaPlayer.setOnCompletionListener {
                            mediaPlayer.release()
                        }
                    }
                }

                else {
                    navigateFinal()
                    if (MainActivity.GlobalState.selectedLanguage == "german") {


                        val mediaPlayer =
                            MediaPlayer.create(context, R.raw.spiel_abgeschlossen) // Soundeffekt)
                        mediaPlayer.start()

                        mediaPlayer.setOnCompletionListener {
                            mediaPlayer.release()
                        }
                    } else {
                        val mediaPlayer =
                            MediaPlayer.create(context, R.raw.game_completed) // Soundeffekt)
                        mediaPlayer.start()

                        mediaPlayer.setOnCompletionListener {
                            mediaPlayer.release()
                        }
                    }
                }
            },

            modifier = Modifier.padding(top = 16.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.arrow),
                contentDescription = "Forward Icon"
            )
        }
    }
}