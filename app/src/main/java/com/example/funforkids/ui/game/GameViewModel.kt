package com.example.funforkids.ui.game

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.funforkids.MainActivity
import com.example.funforkids.data.Repository
import com.example.funforkids.data.local.model.dao.WordE
import com.example.funforkids.data.local.model.dao.WordG
import com.example.funforkids.data.local.model.schema.WordsENG
import com.example.funforkids.data.local.model.schema.WordsGER
import org.jetbrains.exposed.sql.Random
import org.jetbrains.exposed.sql.SortOrder


class GameViewModel(
    savedStateHandle: SavedStateHandle, // Zustand speichern
) : ViewModel() {

    // Zustand für das aktuelle Wort
    var state by mutableStateOf(GameState(requireNotNull(savedStateHandle["difficultyLevel"])))
        private set // Zustand setzen

    private fun generateLetters() {
        // Buchstaben generieren
        val letters = state.currentWord.toCharArray().toMutableList()
        if (state.difficultyLevel == "LEVEL 1") {
            state = state.copy(letters = letters.shuffled().toMutableList())
        } else if (state.difficultyLevel == "LEVEL 2") {
            state = state.copy(letters = letters.shuffled().toMutableList())
        } else {
            for (i in 1..3) {
                val randomChar = ('A'..'Z').random()
                letters.add(randomChar)
            }
            state = state.copy(letters = letters.shuffled().toMutableList())
        }
        state =
            state.copy(playerWord = MutableList(state.currentWord.length) { ' ' }) // Spielerwort generieren
    }

    fun setLetter(index: Int, letter: Char) {
        state = state.copy(
            playerWord = state.playerWord.toMutableList().apply { set(index, letter) },
            letters = state.letters.toMutableList().apply { remove(letter) }
        )
    }


    //**** if gray filed are filled and all letters are  in the correct order ***
    fun checkAndShowNextWord(): Boolean {
        val allLettersCorrect = state.playerWord.joinToString("") == state.currentWord
        val allFieldsFilled = state.playerWord.size == state.currentWord.length
        return allLettersCorrect && allFieldsFilled
    }

    fun generateNextWord() {
        state = state.copy(currentWord = "NEUES_WORT") // Neues Wort generieren
        generateLetters()
    }


    fun generateLastWord(lastWord: String?) {
        if (lastWord != null && state.generateLastWord) {
            Repository.query {


                if (MainActivity.GlobalState.selectedLanguage == "german") {
                    val word = WordG.find { WordsGER.word eq lastWord }.first()
                    state = state.copy(
                        currentWord = word.word,
                        currentImage = word.image.image.bytes,
                        currentAudio = word.audioData.clone()
                    )
                } else {
                    val word = WordE.find { WordsENG.word eq lastWord }.first()
                    state = state.copy(
                        currentWord = word.word,
                        currentImage = word.image.image.bytes,
                        currentAudio = word.audioENG.bytes.clone()
                    )
                }
                generateLetters()
            }
            state.generateLastWord = false
        }
    }
    init {
        Repository.query {
            if (MainActivity.GlobalState.selectedLanguage == "german") {
                val wordsToGo =
                    WordG.all().orderBy(Random() to SortOrder.ASC)
                        .filterNot { MainActivity.GlobalState.successWords.contains(it.word) }
                if (wordsToGo.isEmpty()) {
                    // navigate(//TODO )
                } else {
                    val randomWord = wordsToGo.first()
                    state = state.copy(
                        currentWord = randomWord.word,
                        currentImage = randomWord.image.image.bytes,
                        currentAudio = randomWord.audioData.clone()
                    )
                    generateLetters()
                }
            } else {
                val wordsToGo =
                    WordE.all().orderBy(Random() to SortOrder.ASC)
                        .filterNot { MainActivity.GlobalState.successWords.contains(it.word) }
                if (wordsToGo.isEmpty()) {
                } else {
                    val randomWord = wordsToGo.first()
                    state = state.copy(
                        currentWord = randomWord.word,
                        currentImage = randomWord.image.image.bytes,
                        currentAudio = randomWord.audioENG.bytes.clone()
                    )
                    generateLetters()
                }
            }

        }

    }
}

