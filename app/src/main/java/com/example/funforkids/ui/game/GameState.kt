package com.example.funforkids.ui.game

data class GameState(val difficultyLevel: String,
                     var currentWord: String= "BANANE",
                     val letters: MutableList<Char> = mutableListOf(),
                     val currentImage: ByteArray = ByteArray(0),
                     val currentAudio: ByteArray = ByteArray(0),
                     var isPlaying: Boolean = false,// Neues Feld hinzugefügt
                     val playerWord: List<Char> = listOf(),
                     val successWordList: List<String> = listOf(),
                     var generateLastWord:Boolean = true  //bei falsches Wort wird das letzte Wort nicht generiert

) {

}
