package com.example.funforkids

import FinalScreen_Success
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.funforkids.data.Repository
import com.example.funforkids.data.local.model.dao.WordG
import com.example.funforkids.ui.final.FinalScreen
import com.example.funforkids.ui.final.FinalScreen_Unsuccess
import com.example.funforkids.ui.final.NextLevelScreen
import com.example.funforkids.ui.game.GameScreen
import com.example.funforkids.ui.home.LevelScreen
import com.example.funforkids.ui.theme.FunForKidsTheme

class MainActivity : ComponentActivity() {
    object GlobalState {
        val languages = listOf("german", "english")
        var successWords by mutableStateOf<List<String>>(emptyList()) //globale Liste der richtigen Wörter
        var wordCount: Long = 0 //gesamte Anzahl der Wörter
        var selectedLanguage by mutableStateOf<String>("english")//ausgewählte Sprache
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Repository.Configuration.filesDir = filesDir
        Repository.Configuration.assets = assets // database configuration

        Repository.query {
            GlobalState.wordCount = WordG.count()
            println(WordG.count())
            println(WordG.all().first().word)
        }

        setContent {
            FunForKidsTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    var navController = rememberNavController()//Navigation Controller navigieren zw verschieden Screens
                    NavHost( // wird in Navhost benutzt
                        navController = navController,
                        startDestination = "main"
                    ) { //Navigation zum Hauptmenü
                        //Home Screen
                        //composable ist hier nur ein screen
                        //route ist wie Id damit  er weiss welcher screen er öffent  soll
                        composable(route = "main") {
                            GlobalState.successWords = emptyList()
                            LevelScreen(
                                viewModel(),
                                navigate = { navController.navigate("game/$it") } //Navigation zum Spiel
                                //it bedeutet, dass der Parameter (Level), der an die Funktion übergeben wird, automatisch als 'it' bezeichnet wird
                            )
                        }
                        //Game Screen
                        composable(route = "game/{difficultyLevel}?lastWord={lastWord}") {  //das bedeutet, dass der Schwierigkeitsgrad als Parameter übergeben wird und optional letztes Wort
                                backStackEntry ->  // wie wollen eine variable aus der routh nehmen
                            val lastWord =
                                backStackEntry.arguments?.getString("lastWord") // das letzte Wort aus der Route
                            GameScreen(
                               viewModel(),
                                navigateHome = { navController.navigate("main") },
                                navigate = { navController.navigate(it) },
                                lastWord = lastWord // das letzte Wort als Parameter


                            )
                        }
                        //Final Screen Success

                        composable(route = "final_success/{difficultyLevel}") { backStackEntry ->
                            val difficultyLevel =
                                backStackEntry.arguments?.getString("difficultyLevel") ?: ""
                            FinalScreen_Success(
                                viewModel(),
                                navigate = { navController.navigate("game/" + difficultyLevel) }, // Fügen Sie den 'difficultyLevel' Parameter hinzu
                                navigateNextLevel = {navController.navigate("nextLevel")},
                                difficultyLevel = difficultyLevel,
                                navigateFinal = { navController.navigate("finalScreen") }
                            )
                        }

                        //Final Screen Unsuccess
                        composable(route = "final_unsuccess/{difficultyLevel}?lastWord={lastWord}") { backStackEntry ->
                            val lastWord =
                                backStackEntry.arguments?.getString("lastWord") ?: ""
                            val difficultyLevel =
                                backStackEntry.arguments?.getString("difficultyLevel") ?: ""
                            FinalScreen_Unsuccess(
                                viewModel(),
                                navigateBack = { navController.navigate("game/$difficultyLevel?lastWord=$lastWord")  }, // Korrigieren Sie den Navigationspfad

                            )
                        }
                        composable(route = "nextLevel") {
                            NextLevelScreen(
                                viewModel(),
                                navigateHome = { navController.navigate("main") }
                            )
                        }
                        composable(route = "finalScreen") {
                            FinalScreen(
                                viewModel(),
                                navigateHome = { navController.navigate("main") }

                            )
                        }
                    }
                }
            }
        }
    }
}
